<?php

/**
 * The Follow Me model
 */

if ( ! class_exists( 'Follow_Me_Model' ) ) {

	/**
	 * The Model for the Amelung Follow Me Plugin
	 */
	class Follow_Me_Model {

		private $message;

		public function __construct() {
			$this->message = 'Follow Chris Amelung on Twitter: <a href="https://twitter.com/amelungc">@amelungc</a>';
		}

		public function set_message( $m ) {
			$this->message = 'Follow me on Twitter: <a href="https://twitter.com/' . $m . '">@' . $m . '</a>';
		}

		public function get_message() {
			return $this->message;
		}

	} // end Follow_Me_Model
}