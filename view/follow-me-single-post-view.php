<?php

/**
 * The Single post view
 */

if ( ! class_exists( 'Follow_Me_Single_Post_View' ) ) {

	/**
	 * Class to render the HTML for single posts
	 */
	class Follow_Me_Single_Post_View {

		/**
		 * Render the Twitter message
		 *
		 * @return string
		 */
		public static function render( $message ) {
			// Yes, I know this is rediculous on so many levels. Just proof-of-concept play going on here.
			return "<pre>$message</pre>";
		}

	} // end Follow_Me_Single_Post_View
}