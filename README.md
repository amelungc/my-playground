Amelung Follow Me plugin
==============================

This plugin is nonsense. It displays a "Follow me on Twitter" message on all single posts and goes about it in an overly complex way.

The usefulness of this plugin is not why it exists. I'm experimenting with how I want to structure my code and I have taken this simple concept of using add_filter() to add one line of text to post content and expanded it into an MVC architecture. Beyond overkill, I know.

This idea was mostly-taken from http://www.renegadetechconsulting.com/tutorials/an-mvc-inspired-approach-to-wordpress-plugin-development - not the insanity of making this simple plugin so complex, but the idea and code to use MVC.