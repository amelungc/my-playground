<?php

/*
Plugin Name:  Follow Me on Twitter
Description:  Adds a 'Follow me on Twitter' link to each post
Version:      0.1
Author:       Chris Amelung
Author URI:   https://chrisamelung.com
License:      GPL2
*/

if ( ! class_exists( 'Follow_Me_Controller' ) ) {

	/**
	 * The main plugin controller
	 */
	class Follow_Me_Controller {

		/**
		 * The class constructor
		 */
		public function __construct() {
			if ( ! is_admin() ) {
				add_action( 'wp', array( $this, 'init' ) );
			}
		}

		/**
		 * Callback for the 'wp' action
		 */
		public function init() {
			if ( is_single() ) {
				add_filter( 'the_content', array( &$this, 'render_follow_me_single_post' ) );
			}
		}

		/**
		 * Filter the content on single posts
		 *
		 * @param string $content
		 *
		 * @return string $content with an extra special message
		 */
		public function render_follow_me_single_post( $content ) {

			$plugin_path = plugin_dir_path( __FILE__ );

			// include the model
			require_once( $plugin_path . 'model/follow-me-model.php' );
			$follow_me_model = new Follow_Me_Model();

			// In theory, a set_message() could be run, from an admin page or something, to store a message for display.
			// Nothing like that is being done in this plugin, but the "idea" of it is cool.
			// I'm just using a generic set_message() to assign the Twitter handle to follow. Don't think too hard - it's not logical
			$follow_me_model->set_message( 'amelungc' );

			// get the message
			$message = $follow_me_model->get_message();

			// include the view
			require_once( $plugin_path . 'view/follow-me-single-post-view.php' );

			// render the view
			$content .= Follow_Me_Single_Post_View::render( $message );

			return $content;
		}

	} // end Follow_Me_Controller

	$follow_me = new Follow_Me_Controller;
}

